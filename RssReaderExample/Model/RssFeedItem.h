//
//  RssFeedItem.h
//  RssReaderExample
//
//  Created by Андрей Огородников on 15/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface RssFeedItem : NSManagedObject

+ (instancetype) createOrFetchForLink:(NSString *)link;

@property NSString      *title;
@property NSString      *text;
@property NSString      *link;
@end
