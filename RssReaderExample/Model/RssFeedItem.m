//
//  RssFeedItem.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 15/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "AppDelegate.h"
#import "RssFeedItem.h"

@implementation RssFeedItem

- (NSString *)title {
    return [self valueForKey:@"title"];
}

- (void)setTitle:(NSString *)title {
    [self setValue:title forKey:@"title"];
}

- (NSString *)link {
    return [self valueForKey:@"link"];
}

- (void)setLink:(NSString *)link {
    [self setValue:link forKey:@"link"];
}

- (NSString *)text {
    return [self valueForKey:@"text"];
}

- (void)setText:(NSString *)text {
    [self setValue:text forKey:@"text"];
}

+ (instancetype) createOrFetchForLink:(NSString *)link {
    NSManagedObjectContext *context = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"RssFeedItem"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"link == %@", link];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (error != nil) {
        return [NSEntityDescription insertNewObjectForEntityForName:@"RssFeedItem" inManagedObjectContext:context];
    } else {
        if (results.count == 0) {
            // return a new record
            return [NSEntityDescription insertNewObjectForEntityForName:@"RssFeedItem" inManagedObjectContext:context];
        } else {
            // return fetched record
            return [results firstObject];
        }
    }
}

@end
