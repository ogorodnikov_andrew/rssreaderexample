//
//  RssFeed.h
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface RssFeed : NSManagedObject
@property NSString *title;
@property NSString *link;
+ (instancetype) createOrFetchForLink:(NSString *)link;
@end
