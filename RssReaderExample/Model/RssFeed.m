//
//  RssFeed.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "RssFeed.h"
#import "AppDelegate.h"

@implementation RssFeed

- (NSString *)title {
    return [self valueForKey:@"title"];
}

- (void)setTitle:(NSString *)title {
    [self setValue:title forKey:@"title"];
}

- (NSString *)link {
    return [self valueForKey:@"link"];
}

- (void)setLink:(NSString *)link {
    [self setValue:link forKey:@"link"];
}

+ (instancetype) createOrFetchForLink:(NSString *)link {
    NSManagedObjectContext *context = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"RssFeed"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"link == %@", link];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (error != nil) {
        return [NSEntityDescription insertNewObjectForEntityForName:@"RssFeedItem" inManagedObjectContext:context];
    } else {
        if (results.count == 0) {
            // return a new record
            return [NSEntityDescription insertNewObjectForEntityForName:@"RssFeed" inManagedObjectContext:context];
        } else {
            // return fetched record
            return [results firstObject];
        }
    }
}

@end
