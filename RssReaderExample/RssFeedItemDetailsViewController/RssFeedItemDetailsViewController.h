//
//  RssFeedItemDetailsViewController.h
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "RssFeedItem.h"
#import <UIKit/UIKit.h>

@interface RssFeedItemDetailsViewController : UIViewController

@property (strong,nonatomic) RssFeedItem *rssFeedItem;

@end
