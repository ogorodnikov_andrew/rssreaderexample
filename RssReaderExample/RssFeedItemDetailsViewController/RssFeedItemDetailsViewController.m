//
//  RssFeedItemDetailsViewController.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "RssFeedItemDetailsViewController.h"

@interface RssFeedItemDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation RssFeedItemDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.rssFeedItem.title;
    NSString *htmlString = [NSString stringWithFormat:@"<html><head/><body><h1>%@</h1>%@</body></html>",self.rssFeedItem.title,self.rssFeedItem.text];
    [self.webView loadHTMLString:htmlString baseURL:nil];
}


@end
