//
//  main.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 15/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
