//
//  RssFeedListTableViewConrtoller.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 15/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "ApiManager.h"
#import "RssFeed.h"
#import "RssFeedParser.h"
#import "RssFeedListTableViewConrtoller.h"
#import "RssFeedItemListTableViewController.h"

#define reuseIdentifier @"reuseIdentifier"

@interface RssFeedListTableViewConrtoller ()
@end

@implementation RssFeedListTableViewConrtoller {
    NSArray *rssFeedList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"rss_feed_list_title", nil);
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:reuseIdentifier];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addRssItem)];
    
    [self reloadList];
}

- (void)addRssItem {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"add_rss_feed_item", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = NSLocalizedString(@"rss_feed_url_placeholder", nil);
    }];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok",nil) style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * action){
                                                    UITextField *textField = alert.textFields[0];
                                                    NSString *url = textField.text;
                                                    [[ApiManager sharedManager] loadRssFeedWithURLString:url].then(^(NSData *data){
                                                        return [[RssFeedParser new] parseRssFeedFromURL:url xmlData:data];
                                                    }).then(^{
                                                        [self reloadList];
                                                    }).catch(^(NSError *error){
                                                        NSLog(@"Error:%@",error);
                                                        // todo handle error
                                                    });
                                                }]];
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"cancel",nil) style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action){
                                                [alert dismissViewControllerAnimated:true completion:nil];
                                            }]];
    [self presentViewController:alert animated:true completion:nil];
}

- (void)reloadList {
    [[ApiManager sharedManager] loadRssFeedList].then(^(NSArray *results){
        rssFeedList = results;
        [self.tableView reloadData];
    });
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    RssFeedItemListTableViewController *controller = [RssFeedItemListTableViewController new];
    controller.rssFeed = (RssFeed *)rssFeedList[indexPath.row];
    [self.navigationController pushViewController:controller animated:true ];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[ApiManager sharedManager] deleteRssFeed:rssFeedList[indexPath.row]].then(^{
            [self reloadList];
        });
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return rssFeedList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.textLabel.text = ((RssFeed *)rssFeedList[indexPath.row]).title;
    return cell;
}



@end
