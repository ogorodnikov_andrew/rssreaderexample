//
//  RssFeedParser.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "RssFeed.h"
#import "RssFeedItem.h"
#import "RssFeedParser.h"
#import "AppDelegate.h"

@interface RssFeedParser()<NSXMLParserDelegate>
@end

@implementation RssFeedParser {
    RssFeed *rssFeed;
    
    BOOL isItem;
    NSString *currentItemLink;
    NSString *currentItemTitle;
    NSString *currentItemText;
    
    NSString *currentElement;
    NSManagedObjectContext *context;
    NSMutableSet *items;
}

- (PMKPromise *)parseRssFeedFromURL:(NSString *)url xmlData:(NSData *)xmlData {
    return [PMKPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        NSXMLParser *parser = [[NSXMLParser alloc]initWithData:xmlData];
        context = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
        rssFeed = [RssFeed createOrFetchForLink:url];
        rssFeed.link = url;
        items = [rssFeed mutableSetValueForKey:@"items"];
        isItem = false;
        parser.delegate = self;
        BOOL success = parser.parse;
        if (success) {
            
            NSError *error = nil;
            if (context) [context save:&error];
            resolve(error); // passing nil will resolve the promise with success
        } else {
            resolve(parser.parserError);
        }
    }];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"item"]) {
        isItem = true;
        currentItemLink=  @"";
        currentItemTitle=  @"";
        currentItemText=  @"";
    } else if ([elementName isEqualToString:@"title"]) {
        currentElement = @"title";
    } else if ([elementName isEqualToString:@"link"]) {
        currentElement = @"link";
    } else if ([elementName isEqualToString:@"description"]) {
        currentElement = @"description";
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([currentElement isEqualToString:@"title"]) {
        if (!isItem) {
            // rss feed title
            rssFeed.title = string;
        } else {
            // item title
            currentItemTitle = [currentItemTitle stringByAppendingString:string];
        }
    } else if ([currentElement isEqualToString:@"link"]) {
        if (isItem) {
            // item link
            currentItemLink = string;
        }
    } else if ([currentElement isEqualToString:@"description"]) {
        if (isItem) {
            // item text
            currentItemText = [currentItemText stringByAppendingString:string];
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"item"]) {
        RssFeedItem *item = [RssFeedItem createOrFetchForLink:currentItemLink];
        item.link = currentItemLink;
        item.title = currentItemTitle;
        item.text = currentItemText;
        [items addObject:item];
        currentItemLink=  @"";
        currentItemTitle=  @"";
        currentItemText=  @"";
        currentElement = nil;
        isItem = false;
    } else if ([elementName isEqualToString:@"title"]) {
        currentElement = nil;
    } else if ([elementName isEqualToString:@"link"]) {
        currentElement = nil;
    } else if ([elementName isEqualToString:@"description"]) {
        currentElement = nil;
    }
    
}

@end
