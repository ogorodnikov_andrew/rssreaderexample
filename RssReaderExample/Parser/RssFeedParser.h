//
//  RssFeedParser.h
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PromiseKit/PromiseKit.h>

@interface RssFeedParser : NSObject

- (PMKPromise *)parseRssFeedFromURL:(NSString *)url xmlData:(NSData *)xmlData;

@end
