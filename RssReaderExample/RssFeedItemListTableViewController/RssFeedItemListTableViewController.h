//
//  RssFeedItemListTableViewController.h
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "RssFeed.h"
#import <UIKit/UIKit.h>

@interface RssFeedItemListTableViewController : UITableViewController

@property (nonatomic, strong) RssFeed *rssFeed;

@end
