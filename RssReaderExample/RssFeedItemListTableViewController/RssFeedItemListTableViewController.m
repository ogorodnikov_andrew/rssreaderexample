//
//  RssFeedItemListTableViewController.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 16/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "ApiManager.h"
#import "RssFeedParser.h"
#import "RssFeedItemDetailsViewController.h"
#import "RssFeedItemListTableViewController.h"

#define reuseIdentifier @"reuseIdentifier"

@interface RssFeedItemListTableViewController ()

@end

@implementation RssFeedItemListTableViewController {
    NSArray *items;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = false;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44;
    
    [self.tableView registerClass:UITableViewCell.class forCellReuseIdentifier:reuseIdentifier];
    
    // try to update current feed
    [[ApiManager sharedManager] loadRssFeedWithURLString:self.rssFeed.link].then(^(NSData *data){
        return [[RssFeedParser new] parseRssFeedFromURL:self.rssFeed.link xmlData:data];
    }).then(^{
        [self reloadItems];
    }).catch(^(NSError *error){
        NSLog(@"Error:%@",error);
        // todo handle error
        // load cached items even on network error
        [self reloadItems];
    });
}

- (void)reloadItems {
    [[ApiManager sharedManager] loadRssFeedItemListForRssFeed:self.rssFeed].then(^(NSMutableSet *results){
        items = results.allObjects;
        [self.tableView reloadData];
    });
}

- (void)setRssFeed:(RssFeed *)rssFeed {
    _rssFeed = rssFeed;
    self.title = rssFeed.title;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text = ((RssFeedItem *)items[indexPath.row]).title;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    RssFeedItemDetailsViewController *controller = [RssFeedItemDetailsViewController new];
    controller.rssFeedItem = (RssFeedItem *)items[indexPath.row];
    [self.navigationController pushViewController:controller animated:true ];
}

@end
