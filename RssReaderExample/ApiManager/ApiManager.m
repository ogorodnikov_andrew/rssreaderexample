//
//  ApiManager.m
//  RssReaderExample
//
//  Created by Андрей Огородников on 15/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//


#import "ApiManager.h"
#import "RssFeedItem.h"
#import "AppDelegate.h"
#import "RssFeedParser.h"

@implementation ApiManager

+ (instancetype)sharedManager {
    static ApiManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [ApiManager new];
    });
    return _sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setResponseSerializer:[AFHTTPResponseSerializer new]];
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/rss+xml"];
    }
    return self;
}

- (PMKPromise *)loadRssFeedList {
    return [PMKPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        NSManagedObjectContext *context = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
        NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"RssFeed"];
        NSError *error = nil;
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error == nil) {
            resolve(results);
        }
        else resolve(error);
    }];
}

- (PMKPromise *)loadRssFeedItemListForRssFeed:(RssFeed *)rssFeed {
    return [PMKPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        NSMutableSet *rssFeedItemsSet = [rssFeed mutableSetValueForKey:@"items"];
        resolve(rssFeedItemsSet);
    }];
}

- (PMKPromise *)loadRssFeedWithURLString:(NSString *)urlString {
    return [PMKPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        [self GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            resolve(responseObject);
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            resolve(error);
        }];
    }];
}

- (PMKPromise *)createOrUpdateRssFeedWithTitle:(NSString *)title andLink:(NSString *)link {
    return [PMKPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        NSManagedObjectContext *context = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
        NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"RssFeed"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"link == %@", link];
        [request setPredicate:predicate];
        NSError *error = nil;
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error != nil) {
            resolve(error);
        } else {
            if (results.count == 0) {
                // create a new record with title and link
                RssFeed *rssFeed = [NSEntityDescription insertNewObjectForEntityForName:@"RssFeed" inManagedObjectContext:context];
                rssFeed.title = title;
                rssFeed.link = link;
            } else {
                // update title
                RssFeed *fetchedRssFeed = [results firstObject];
                fetchedRssFeed.title = title;
            }
            [context save:&error];
            resolve(error); // passing nil will resolve the promise with success
        }
    }];
}

- (PMKPromise *)deleteRssFeed:(RssFeed *)rssFeed {
    return [PMKPromise promiseWithResolverBlock:^(PMKResolver resolve) {
        NSManagedObjectContext *context = ((AppDelegate *)[UIApplication sharedApplication].delegate).managedObjectContext;
        [context deleteObject:rssFeed];
        NSError *error = nil;
        [context save:&error];
        resolve(error);
    }];
}

@end
