//
//  ApiManager.h
//  RssReaderExample
//
//  Created by Андрей Огородников on 15/05/15.
//  Copyright (c) 2015 Андрей Огородников. All rights reserved.
//

#import "RssFeed.h"
#import "AFNetworking.h"
#import <Foundation/Foundation.h>
#import <PromiseKit/PromiseKit.h>

@interface ApiManager : AFHTTPSessionManager
    
+ (instancetype)sharedManager;

- (PMKPromise *)createOrUpdateRssFeedWithTitle:(NSString *)title andLink:(NSString *)link ;

- (PMKPromise *)loadRssFeedList;

- (PMKPromise *)loadRssFeedItemListForRssFeed:(RssFeed *)rssFeed;

- (PMKPromise *)loadRssFeedWithURLString:(NSString *)urlString;

- (PMKPromise *)deleteRssFeed:(RssFeed *)rssFeed;

@end
